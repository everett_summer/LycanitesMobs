package com.lycanitesmobs.client.obj;


import net.minecraft.util.math.vector.Vector3f;

public class Mesh
{
    public int[] indices;
    public Vertex[] vertices;
    public Vector3f[] normals;
}
